extends Node2D


func _on_submit_pressed():
	
	var data = {
		"member_id": "player1",
		"score": randi_range(500,5000)
	}
	
	print(data)
	
	var score = await LootLocker.submit_score(data)
	$Response.text = "%s" % score


func _on_scores_pressed():
	
	var scores = await LootLocker.get_scores()
	
	$Response.text = "%s" % scores
