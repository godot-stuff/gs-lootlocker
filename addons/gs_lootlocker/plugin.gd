@tool
extends EditorPlugin


func _enter_tree():
	add_autoload_singleton("LootLocker", "res://addons/gs_lootlocker/lootlocker.tscn")
	
	
func _exit_tree():
	remove_autoload_singleton("LootLocker")
