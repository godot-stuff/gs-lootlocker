#
# Copyright 2023 SpockerDotNet LLC, All rights reserved.
#

extends Node 

const VERSION = "4.2-DEV"

var player_session_exists: bool
var player_identifier: String
var lootlocker_api_key: String = "dev_0e751be63e5e4c96b29750d4287f66bd"
var lootlocker_leaderboard_key: String = "leaderboardKey"
var lootlocker_mode: bool = true

var http_request : HTTPRequest
var settings

func _init():
	
	http_request = HTTPRequest.new()
	http_request.name = "LootLocker(HTTPRequest)"
	http_request
	add_child(http_request)

func _ready():
	
	print()
	print("godot-stuff LootLocker")
	print("https://gitlab.com/godot-stuff/gs-lootlocker")
	print("Copyright 2024, SpockerDotNet LLC")
	print("Version " + VERSION)
	print()

func submit_score(data):
	# submit score
	var session_token = await get_session_token()
	var http_request_headers = ["Content-Type: application/json", "x-session-token:" + session_token]
	print(http_request_headers)
	
	http_request.request("https://api.lootlocker.io/game/leaderboards/" + lootlocker_leaderboard_key + "/submit", http_request_headers, HTTPClient.METHOD_POST, JSON.stringify(data))
	var submit_response = await http_request.request_completed
	print("submit done")
	print(submit_response)
	var json = JSON.new()
	var score = json.parse(submit_response[3].get_string_from_utf8())
	print(json.get_data())
	return json.get_data()

func get_scores():
	
	var session_token = await get_session_token()
	var http_request_headers = ["Content-Type: application/json", "x-session-token:" + session_token]
	print(http_request_headers)
	
	http_request.request("https://api.lootlocker.io/game/leaderboards/" + lootlocker_leaderboard_key + "/list?count=10", http_request_headers, HTTPClient.METHOD_GET)
	var submit_response = await http_request.request_completed
	print("submit done")
	print(submit_response)
	var json = JSON.new()
	var body = json.parse(submit_response[3].get_string_from_utf8())
	print(json.get_data())
	return json.get_data()
	
func get_session_token():
	var http_request_headers = ["Content-Type: application/json"]
	
	# get session
	
	var auth = { "game_key": lootlocker_api_key, "game_version": "0.0.0.1", "development_mode": true }
	http_request.request("https://api.lootlocker.io/game/v2/session/guest", http_request_headers, HTTPClient.METHOD_POST, JSON.stringify(auth))
	var auth_response = await http_request.request_completed
	print("auth done")
	print(auth_response)
	var json = JSON.new()
	json.parse(auth_response[3].get_string_from_utf8())
	var session_token = json.get_data().session_token
#	var auth_token = json.get_data().auth_token
	# Print server response
	print(json.get_data())
	return session_token	
